# dfcheck

dfcheck is a command line utility for unix systems that checks free disk space and signals via its exit code whether there was at least as much space as requested.

This is useful for use in cronjobs like `dfcheck --free --at-least=10% /var || rm -rf /var/cache/foo/*`

## Usage

```bash
$ dfcheck --available --at-least=5% /var || echo "Free some stuff!"
```

```bash
$ dfcheck --free --at-least=10% --print-on-failure /tmp || echo "Free some stuff in /tmp!"
```

```bash
$ dfcheck --used --at-most=90% || echo "Getting a bit tight on /!"
```

```bash
$ dfcheck --available --at-least=5G /backups -v && do_backup.sh
```

```bash
$ dfcheck --available --at-least=5G -v
/: total 376.4 GiB, free 57.5 GiB (15.3%), available 38.3 GiB (10.2%), used 319.0 GiB (84.7%)
Check if at least 5.0 GiB available: true
```

## Known Issues

Will likely only build and run on Linux at the moment owing to the use of libc::statvfs().

## License

Licensed under the MIT license.
