extern crate clap;
use clap::{App, Arg, ArgGroup};

use std::fmt;
use std::path::PathBuf;
use std::process;

extern crate nix;
use nix::sys::statvfs::*;

extern crate unbytify;
use unbytify::*;

#[derive(Copy, Clone)]
enum CompareType {
    AtLeast,
    AtMost,
}

impl fmt::Display for CompareType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let display_desc = match *self {
            CompareType::AtLeast => "at least",
            CompareType::AtMost => "at most",
        };
        write!(f, "{}", display_desc)
    }
}

fn fmt_size(byte_size: u128) -> String {
    let (num, unit) = bytify(byte_size as u64);

    format!("{:.1} {}", num, unit)
}

fn main() {
    // command line arguments
    let matches = App::new("dfcheck")
        .version("0.1")
        .author("Tim-Philipp Müller <tim centricular com>")
        .about("Check free/used disk space on file system")
        .arg(
            Arg::with_name("at-least")
                .long("at-least")
                .value_name("VALUE")
                .help("Check if at least VALUE disk space is free or used. Can specify % or units k/M/G/T")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("at-most")
                .long("at-most")
                .value_name("VALUE")
                .help("Check if at most VALUE disk space is free or used. Can specify % or units k/M/G/T")
                .takes_value(true),
        )
        .arg(Arg::with_name("available")
                 .long("available")
                 .help("Check space on file system available to non-privileged user"),
        )
        .arg(Arg::with_name("free")
                 .long("free")
                 .help("Check free space on file system"),
        )
        .arg(Arg::with_name("used").long("used").help(
            "Check used space on file system",
        ))
        .arg(Arg::with_name("verbose").short("v").long("verbose").help(
            "Print information (always)",
        ))
        .arg(Arg::with_name("print-on-failure").short("p").long("print-on-failure").help(
            "Print information (only if the check fails)",
        ))
        .group(ArgGroup::with_name("comparison")
                         .args(&["at-least", "at-most"])
                         .required(true))
        .group(ArgGroup::with_name("check-type")
                         .args(&["available", "used", "free"])
                         .required(true))
        .arg(Arg::with_name("directory"))
        .after_help("Checks free/used/available disk space on file system and signal if condition was met or not via exit code, which is useful in shell scripts or cron jobs.")
        .get_matches();

    let verbose = matches.is_present("verbose");
    let print_on_failure = matches.is_present("print-on-failure");

    let dir = PathBuf::from(matches.value_of("directory").unwrap_or("/"));

    let stats = statvfs(&dir).unwrap_or_else(|err| {
        eprintln!(
            "Could not query file system stats for path {:?}: {}",
            dir, err
        );
        process::exit(2);
    });

    let frag_size: u128 = stats.fragment_size() as u128;
    let total_size: u128 = stats.blocks() as u128 * frag_size;
    let avail_size: u128 = stats.blocks_available() as u128 * frag_size;
    let free_size: u128 = stats.blocks_free() as u128 * frag_size;
    let used_size = total_size - free_size;
    let avail_perc = avail_size as f64 * 100.0 / total_size as f64;
    let free_perc = free_size as f64 * 100.0 / total_size as f64;
    let used_perc = used_size as f64 * 100.0 / total_size as f64;

    let (check_val_string, cmp_type) = if let Some(arg) = matches.value_of("at-least") {
        (arg, CompareType::AtLeast)
    } else if let Some(arg) = matches.value_of("at-most") {
        (arg, CompareType::AtMost)
    } else {
        unreachable!();
    };

    let cur_vals = if matches.is_present("used") {
        (used_size, used_perc, "used")
    } else if matches.is_present("available") {
        (avail_size, avail_perc, "available")
    } else if matches.is_present("free") {
        (free_size, free_perc, "free")
    } else {
        unreachable!();
    };

    let percentage = check_val_string.ends_with('%');

    let cur_val = if percentage {
        cur_vals.1 as f64
    } else {
        cur_vals.0 as f64
    };

    let check_val = if percentage {
        let len = check_val_string.len();
        check_val_string[0..len - 1]
            .parse::<f64>()
            .unwrap_or_else(|err| {
                eprintln!(
                    "Could not parse string {} as numeric percentage value: {}",
                    check_val_string, err
                );
                process::exit(99);
            })
    } else {
        unbytify(check_val_string).unwrap_or_else(|err| {
            eprintln!(
                "Could not parse string {} as human byte size value: {}",
                check_val_string, err
            );
            process::exit(99);
        }) as f64
    };

    let cmp_result = match cmp_type {
        CompareType::AtLeast => cur_val >= check_val,
        CompareType::AtMost => cur_val <= check_val,
    };

    let check_desc = cur_vals.2; // "free", "available", "used"

    if verbose || (print_on_failure && !cmp_result) {
        println!(
            "{}: total {}, free {} ({:.1}%), available {} ({:.1}%), used {} ({:.1}%)",
            dir.to_string_lossy(),
            fmt_size(total_size),
            fmt_size(free_size),
            free_perc,
            fmt_size(avail_size),
            avail_perc,
            fmt_size(used_size),
            used_perc
        );

        if percentage {
            println!(
                "Check if {} {:.1}% {}: {}",
                cmp_type, check_val, check_desc, cmp_result
            );
        } else {
            println!(
                "Check if {} {} {}: {}",
                cmp_type,
                fmt_size(check_val as u128),
                check_desc,
                cmp_result
            );
        }
    }

    if cmp_result {
        process::exit(0);
    } else {
        process::exit(1);
    }
}
